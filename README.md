This directory is only for sync reasons for files that are provided on
polyasite.unibas.ch.

Go to our [polyASite](http://polyasite.unibas.ch) to learn about the data provided here.
